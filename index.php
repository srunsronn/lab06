<?php
$message = ''; 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    include 'db-connection.php';

    $phone_number = $con->real_escape_string($_POST['phonenumber']);
    $firstname = $con->real_escape_string($_POST['firstname']);
    $lastname = $con->real_escape_string($_POST['lastname']);
    $url_fb_social = $con->real_escape_string($_POST['url_fb_social']);
    
    // File upload
    if (isset($_FILES["picture_profile"]) && $_FILES["picture_profile"]["error"] == 0) {
        $target_directory = "uploads/";
        $target_file = $target_directory . basename($_FILES["picture_profile"]["name"]);
        
        if (move_uploaded_file($_FILES["picture_profile"]["tmp_name"], $target_file)) {
            $message = "The file has been uploaded.";
            $picture_profile = $target_file; 
            
            // Table name 'phone_contact'
            $sql = "INSERT INTO phone_contact (phone_number, firstname, lastname, url_fb_social, picture_profile) VALUES (?, ?, ?, ?, ?)";
            
            if ($stmt = $con->prepare($sql)) {
                $stmt->bind_param("sssss", $phone_number, $firstname, $lastname, $url_fb_social, $picture_profile);
                
                if ($stmt->execute()) {
                    $message = "Contact saved successfully.";
                } else {
                    $message = "Error: " . $stmt->error;
                }
                
                $stmt->close();
            } else {
                $message = "Error preparing statement: " . $con->error;
            }
        } else {
            $message = "Sorry, there was an error uploading your file.";
        }
    } else {
        $message = "No file was uploaded or there was an upload error.";
    }

    $con->close();
}
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <?php if($message != ''): ?>
            <div class="alert alert-info">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        
        <h2>Upload to Database</h2>
        <form action="" method="post" enctype="multipart/form-data">            
            <div class="form-group">
                <label for="phonenumber">Phone Number</label>
                <input type="text" class="form-control" name="phonenumber" required>
            </div>
            <div class="form-group">
                <label for="firstname">First Name</label>
                <input type="text" class="form-control" name="firstname" required>
            </div>
            <div class="form-group">
                <label for="lastname">Last Name</label>
                <input type="text" class="form-control" name="lastname" required>
            </div>
            <div class="form-group">
                <label for="url_fb_social">URL FB</label>
                <input type="text" class="form-control" name="url_fb_social" required>
            </div>
            <div class="form-group">
                <label for="profile_picture">Profile Picture</label>
                <input type="file" class="form-control" name="profile_picture" required>
            </div>

            <button type="submit" class="btn btn-primary">Upload</button>
        </form>
    </div>
</body>
</html>
